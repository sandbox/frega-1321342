<?php
/**
 * @file
 * dnode interface.
 */

/**
 * Every DrupalDnode strategy has implement this interface
 *
 * Currently it only consists of one method.
 */
interface DrupalDnodeStrategyInterface {
  /**
   * RPC Call to dnode server
   * @abstract
   *
   * @param string $server_id
   *   serverId of the server to call
   * @param string $method
   *   method on the server to call
   * @param array $arguments
   *   arguments to be passed to that function
   * @param array $options
   *   request options
   *
   * @return DrupalDnodeRequest
   *   Returns a DrupalDnodeRequest instance
   */
  public function rpc($server_id, $method, $arguments, $options = array());

  /**
   * Like rpc but with this signature
   * serverId, method, arg1, arg2, arg3, ..., callback/options
   *
   * Difference to DrupalDnodeStrategyInterface::rpc - it expects arguments like
   * call_user_func() i.e. not in an array.
   *
   * @return DrupalDnodeRequest
   *   Returns a DrupalDnodeRequest instance
   */
  public function rpcCallback();
}
