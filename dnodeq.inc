<?php
/**
 * @file
 * Helper and utility functions for dnodeq.
 */

/**
 * Helper function invoking responses picked up from a response queue
 *
 * @param array $responses
 *   Responses
 * @param string $callback
 *   Callback optional
 *
 * @return int
 *   Number of responses.
 */
function _dnodeq_handle_responses($responses, $callback = NULL) {
  if (sizeof($responses)) {
    foreach ($responses as $response) {
      if ($callback) {
        $callback($response['request'], $response['reponse'], $response['options']);
      }
      elseif (isset($response['options']['module']) && is_callable($response['options']['module'] . '_dnode_response')) {
        $func = $response['options']['module'] . '_dnode_response';
        $func($response['request'], $response['reponse'], $response['options']);
      }
      else {
        module_invoke_all('dnode_response', $response['request'], $response['reponse'], $response['options']);
      }
    }
  }
  // Pff, what to return here.
  return sizeof($responses);
}

/**
 * Retrieve queued responses
 *
 * @param string $queue
 *   Which queue to retrieve.
 * @param int $limit
 *   How many items to retieve.
 */
function _dnodeq_get_queued_responses($queue = 'dnodeq-response', $limit = 10) {
  $options = array('queue' => $queue, 'limit' => 10);
  // We need this to be synchronous ...
  $request = dnode_rpc('dnodeq', 'getRpcResponses', array($options), array(), 'dnode_http');
  $response = $request->getResponseObject()->getValues();
  // Returns (err, data)
  return ($response[0]) ? FALSE : $response[1];
}
