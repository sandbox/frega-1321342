<?php
/**
 * @file
 * dnode request class.
 */

class DrupalDnodeResponse {
  protected $responseValue;
  protected $mappedResponseValue;
  protected $requestObject;

  /**
   * Factory function that constructs a DrupalDnodeResponse instance for a
   * request id.
   * @static
   *
   * @param string $request_id
   *   Request id to construct Response object for.
   *
   * @return bool|DrupalDnodeResponse
   *   Returns FALSE if no request with given request id exists.
   */
  static function retrieveByRequestId($request_id) {
    if ($request = DrupalDnodeRequest::retrieveByRequestId($request_id)) {
      return $request->getResponseObject();
    }
    else {
      return FALSE;
    }
  }

  /**
   * Constructor
   *
   * @param mixed $values
   *   Response values
   * @param DrupalDnodeRequest $request_object
   *   Request object
   *
   * @return void
   *   Constructor
   */
  function __construct($values, $request_object = NULL) {
    $this->responseValue = $values;
    if ($request_object) {
      $this->requestObject = $request_object;
    }
  }

  /**
   * Get request id
   */
  public function getRequestId() {
    return isset($this->requestObject) ? $this->requestObject->getRequestId() : NULL;
  }

  /**
   * Get request object.
   */
  public function getRequestObject() {
    if (isset($this->requestObject)) {
      return $this->requestObject;
    }
    elseif ($this->getRequestId()) {
      // Retrieve by request id.
      return $this->requestObject = DrupalDnodeRequest::retrieveByRequestId($this->getRequestId());
    }
    else {
      // We cannnot retrieve the matching request object.
      return FALSE;
    }
  }

  /**
   * Set associated request object
   */
  public function setRequestObject(DrupalDnodeRequest $request) {
    $this->requestObject = $request;
  }

  /**
   * Set responseValue request object
   */
  public function setResponseValue($response) {
    $this->responseValue = $response;
  }

  /**
   * Get mapped values
   */
  public function getMappedValues($offset = NULL) {
    if (!isset($this->mappedResponseValue)) {
      $options = isset($this->requestObject) ? $this->requestObject->options : array();
      $map = isset($options['mapResponseValues']) ? $options['mapResponseValues'] : array();
      foreach ($map as $i => $key) {
        $this->mappedResponseValue[$key] = isset($this->responseValue[$i]) ? $this->responseValue[$i] : NULL;
      }
    }
    if (isset($offset)) {
      return isset($this->mappedResponseValue[$offset]) ? $this->mappedResponseValue[$offset] : NULL;
    }
    else {
      return $this->mappedResponseValue;
    }
  }

  /**
   * Get response values
   */
  public function getValues($offset = NULL) {
    if (isset($offset)) {
      if (is_numeric($offset)) {
        return isset($this->responseValue[$offset]) ? $this->responseValue[$offset] : NULL;
      }
      else {
        return $this->getMappedValues($offset);
      }
    }
    else {
      return $this->responseValue;
    }
  }

  /**
   * Get value by offset
   */
  public function getValue($offset) {
    return $this->getValues($offset);
  }

  /**
   * Get mapped value by key.
   */
  public function getMappedValue($offset) {
    return $this->getMappedValue($offset);
  }
}
