<?php
/**
 * @file
 * API Documentation of dnode.
 */

/**
 * Declare which dnode_servers/clients/plugins are available / provided.
 * array(
 *  'machine_name_of_the_server' =>
 *    array(
 *      'description' => t('Example description'),
 *      'type' => 'php|ssjs',
 *      'port' => 9999
 *      @todo: socket path
 *      // configuration
 *      optional:
 *      'config' => array(),
 *      'config_replace' => FALSE|TRUE - if TRUE then the configuration will
 *        be replace rather than merged "over" the defaults.
 *    )
 * );
 */
function hook_dnode_info() {
}

/**
 * Alter which dnode servers are available
 */
function hook_dnode_info_alter(&$items) {
}

/**
 * Declare a strategy, i.e. concerte "communication" implementation
 *
 * array(
 *  'machine_name_of_the_strategy' =>
 *    array(
 *      'description' => t('Example description'),
 *      'class' => 'DrupalDnodeq'
 *    )
 * );
 */
function hook_dnode_strategy_info() {
}

/**
 * Alter which strategies are available
 */
function hook_dnode_strategy_info_alter() {
}

/**
 * Handle incoming (delayed/queued) rpc-response from dnode/nodejs.
 *
 * @param DrupalDnodeRequest $request
 *   request information
 * @param DrupalDnodeResponse $response
 *   response information
 */
function hook_dnode_response($request, $response) {
}

/**
 * Alter incoming (delayed/queued) rpc-response from dnode/nodejs.
 *
 * @param DrupalDnodeRequest $request
 *   request information
 * @param DrupalDnodeResponse $response
 *   response information
 */
function hook_dnode_response_alter(&$request, &$response) {
}


/**
 * Handle incoming messages from dnode/nodejs.
 *
 * @param string $event
 *   Event
 * @param mixed  $data
 *   Message data
 */
function hook_dnode_message($event, $data) {
}
