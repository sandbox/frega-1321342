<?php
/**
 * @file
 * dnode utility functions.
 */

/**
 * Assemble an array of server configuration. Combine file-based configuration
 * from e.g. config.json in the ssjs/ directories with overrides.
 *
 * @param array $options
 *   options
 *   - filter_by_id: filter by a single server.
 *   - verbose: show verbosely how the configuration is assembled.
 *
 * @return array|null
 *   array of configuration
 */
function dnode_get_server_configuration($options = array()) {
  $filter_by_id = isset($options['filter_by_id']) ? $options['filter_by_id'] : FALSE;
  // Get servers.
  $dnode_servers = dnode_get_dnode_info();
  if ($filter_by_id) {
    if (isset($dnode_servers[$filter_by_id])) {
      $dnode_servers = array($filter_by_id => $dnode_servers[$filter_by_id]);
    }
    else {
      return NULL;
    }
  }
  $configuration = array();
  foreach ($dnode_servers as $id => $info) {
    $type = NULL;
    $config = array();
    // See if we have a config section.
    $config_drupal = isset($info['config']) ? $info['config'] : FALSE;
    if (isset($info['config_file']) && $info['config_file']) {
      if (file_exists($info['module_path'] . '/' . $info['config_file'])) {
        $contents = file_get_contents($info['module_path'] . '/' . $info['config_file']);
        $config = json_decode($contents, TRUE);
        $type = 'config:' . $info['config_file'];
      }
    }
    elseif (isset($info['type']) && $info['type'] == 'ssjs' && file_exists($info['module_path'] . '/ssjs/config.json')) {
      $contents = file_get_contents($info['module_path'] . '/' . '/ssjs/config.json');
      $config = json_decode($contents, TRUE);
      $type = 'config:ssjs/config.json';
    }
    else {
      $type = 'config:no-ssjs-config';
    }
    $config_ssjs = $config;
    if ($config_drupal) {
      // Merge or replace configuration.
      $config_replace = isset($info['config_replace']) && $info['config_replace'];
      if ($config_replace) {
        $config = $config_drupal;
        $type = 'drupal-config replaced';
      }
      else {
        $config = array_merge($config_ssjs, $config_drupal);
        $type .= ' (drupal-config merged)';
      }

    }
    else {
      $config = $config_ssjs;
    }
    if ($config || $options['verbose']) {
      if (isset($options['verbose'])) {
        $configuration[$id]['ssjs_config'] = $config_ssjs;
        $configuration[$id]['drupal_config'] = $config_drupal;
        $configuration[$id]['info'] = $info;
        $configuration[$id]['source'] = $type;
        $configuration[$id]['config'] = $config;
      }
      else {
        $configuration[$id] = $config;
      }
    }
  }
  if ($filter_by_id) {
    return isset($configuration[$filter_by_id]) ? $configuration[$filter_by_id] : NULL;
  }
  return $configuration;
}

/**
 * Generates a unique request_id
 *
 * @return string
 *   Returns token for the current request.
 */
function dnode_generate_request_id() {
  return drupal_get_token(session_id() . user_password() . $_SERVER['REQUEST_TIME']);
}

/**
 * Builds a callback url.
 *
 * @param string $request_id
 *   Request id (from dnode_generate_request_id).
 * @param string $module
 *   Module handling the callback (defaults to dnode)
 *
 * @return string
 *   Absolute URL for callback.
 */
function dnode_generate_callback_url($request_id, $module) {
  return url('dnode/callback/' . $module . '/' . $request_id, array('absolute' => TRUE));
}

/**
 * Create the request entry.
 *
 * @param string $server_id
 *   server id
 * @param string $method
 *   method
 * @param array $args
 *   arguments array
 * @param array $options
 *   request options
 *
 * @return bool|stdClass
 *   False if request could not be created or a stdClass object persisted in
 *   database.
 */
function dnode_create_request($server_id, $method, $args, $options) {
  $obj = new stdClass();

  $obj->request_id = isset($options['request_id']) ? $options['request_id'] : dnode_generate_request_id();
  $obj->request = serialize(array(
    'serverId'=>$server_id,
    'method' => $method,
    'arguments' => $args,
    'options' => $options,
  ));
  $obj->status = DNODE_REQUEST_STATUS_PENDING;
  $obj->module = isset($options['module']) ? $options['module'] : 'dnode';
  $obj->ctime = $_SERVER['REQUEST_TIME'];
  if (!drupal_write_record('dnode_requests', $obj)) {
    return FALSE;
  }
  else {
    return $obj;
  }
}

/**
 * Update the request entry with the response.
 *
 * @param string $request_id
 *   request id
 * @param array $response
 *   response to be stored for this request id
 * @param int $status
 *   status to set
 *
 * @return bool
 *   TRUE on success, or FALSE on failure.
 */
function dnode_update_response($request_id, $response, $status) {
  return db_update('dnode_requests')
    ->fields(array(
      'response' => serialize($response),
      'rtime' => $_SERVER['REQUEST_TIME'],
      'status' => $status,
    ))
    ->condition('request_id', $request_id)
    ->execute();
}

/**
 * Loads a request entry from the database.
 *
 * @params array options
 *   options specifying which request to retrieve
 *   - request_id - request id
 *   - module - module
 *   - status - status
 *
 *   - multiple - return an array of each results or just one.
 *
 * @return array
 *   Single array if option multiple not set.
 */
function dnode_load_request($options = array()) {
  $q = db_select('dnode_requests', 'd')
    ->fields('d');
  if (isset($options['request_id'])) {
    $q->condition('request_id', $options['request_id']);
  }
  if (isset($options['module'])) {
    $q->condition('module', $options['module']);
  }
  if (isset($options['status'])) {
    $q->condition('status', $options['status']);
  }
  // Return an array or only the first row.
  if (isset($options['multiple'])) {
    return $q->execute()->fetchAll();
  }
  else {
    return $q->execute()->fetchAssoc();
  }
}
