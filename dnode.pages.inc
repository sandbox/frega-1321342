<?php
/**
 * @file
 * Page callbacks for dnode.
 */

/**
 * Page callback for incoming responses via reponseHttpCallback or
 * dnodeq.sendMessageToDrupal
 */
function dnode_http_callback($type, $module, $request_id = FALSE) {
  switch ($type) {
    case 'callback':
      $request = DrupalDnodeRequest::retrieveByRequestId($request_id);
      if ($request) {
        // Let's grab the raw POST input.
        $r = file_get_contents('php://input');
        $values = drupal_json_decode($r);
        // @todo: properly tell if there's an error?
        $error = (is_object($values) && isset($values->error) && $values->error)
          || (is_array($values) && isset($values['error']) && $values['error']);

        // Update request data.
        $request->setResponseValue($values);
        // Reload updated request.
        $response = $request->getResponseObject($values);
        dnode_invoke_response($request, $response);
        drupal_json_output(array());
      }
      else {
        drupal_access_denied();
      }
      break;

    case 'message':
      if (isset($_SERVER['HTTP_X_DNODE_SERVICE_KEY']) && $_SERVER['HTTP_X_DNODE_SERVICE_KEY'] == variable_get('dnode_service_key', 'change me')) {
        $r = file_get_contents('php://input');
        $values = drupal_json_decode($r);
        dnode_invoke_message($module, $values);
        drupal_json_output(array());
      }
      else {
        drupal_access_denied();
      }
      break;

    default:
      drupal_not_found();
  }
}
