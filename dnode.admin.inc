<?php
/**
 * @file
 * Admin page callback file for the dnode module.
 */

/**
 * Form builder; Return form for dnode settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function dnode_settings() {
  // dnode_default_strategy
  $form = array();
  $options = dnode_get_dnode_strategy_info();
  $opts = array(
    FALSE => t('No default dnode strategy (order: dnode-php, dnode_http)'),
  );
  foreach ($options as $id => $info) {
    $opts[$id] = $info['description'];
  }
  $form['dnode_default_strategy'] = array(
    '#type' => 'select',
    '#options' => $opts,
    '#title' => t('Default strategy'),
    '#default_value' => variable_get('dnode_default_strategy', FALSE),
    '#description' => t('Select the default strategy'),
    '#required' => TRUE,
  );

  // Configure dnode_requests_ttl.
  $form['dnode_requests_ttl'] = array(
    '#type' => 'text',
    '#title' => t('TTL for dnode_requests (in seconds)'),
    '#default_value' => variable_get('dnode_requests_ttl', 86400 * 30),
    '#required' => FALSE,
  );
  // Configure dnode service_key.
  $form['dnode_service_key'] = array(
    '#type' => 'text',
    '#title' => t('Service key used for authorising nodejs->drupal requests'),
    '#default_value' => variable_get('dnode_service_key', 'change me'),
    '#required' => TRUE,
  );
  $form['dnode_configuration'] = array(
    '#title' => t('dnode server configuration'),
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#description' => t('To override the configuration below implement hook_dnode_info_alter in a module and set the values for "config"-key accordingly.'),
  );
  module_load_include('inc', 'dnode');
  $configuration = dnode_get_server_configuration(array('verbose' => TRUE));
  foreach ($configuration as $id => $info) {
    $title = $id;
    if (isset($info['info']['description'])) {
      $title .= ': ' . $info['info']['description'];
    }
    // Allow the configuration of each server/module to be reviewed.
    $form['dnode_configuration'][$id] = array(
      '#title' => $title,
      '#type' => 'fieldset',
      '#description' => t('Configuration for @server (source: @source): <pre>@config</pre>',
        array(
          '@server' => $id,
          '@source' => isset($info['source']) ? $info['source'] : '',
          '@config'=> isset($info['config']) ? print_r($info['config'], TRUE) : t('No configuration'),
        )
      ),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
  }


  return system_settings_form($form);
}
