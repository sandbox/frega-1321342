<?php
/**
 * @file
 * dnode request class.
 */

class DrupalDnodeRequest {
  protected $strategy = NULL;
  protected $serverId = NULL;
  protected $method = NULL;
  protected $arguments = NULL;

  protected $record = NULL;

  protected $isDeferred = FALSE;
  protected $requestId = NULL;

  protected $responseValue = NULL;
  protected $responseObject = NULL;

  public $callback = NULL;
  public $options = array();

  /**
   * Constructor
   *
   * @param string $strategy
   *   Strategy used in request.
   * @param string $server_id
   *   Server id.
   * @param string $method
   *   Remote method called.
   * @param array $arguments
   *   Arguments passed to method.
   * @param array $options
   *   Reqeust and and response options.
   *
   * @return void
   *   Constructor.
   */
  public function __construct($strategy, $server_id, $method, $arguments, $options = array()) {
    $this->strategy = $strategy;
    $this->serverId = $server_id;
    $this->method = $method;
    $this->arguments = $arguments;
    $this->handleOptions($options);
  }

  /**
   * Factory function that constructs a DrupalDnodeRequest instance from a
   * request id.
   *
   * @param string $request_id
   *   Request id to construct Request object for.
   *
   * @return bool|DrupalDnodeRequest
   *   Returns FALSE if no request with given request id exists.
   */
  static function retrieveByRequestId($request_id) {
    $record = (object) dnode_load_request(array('request_id' => $request_id));
    if (!$record) {
      return FALSE;
    }
    $info = unserialize($record->request);
    $request = new DrupalDnodeRequest($info['options']['strategy'], $info['serverId'], $info['method'], $info['arguments'], $info['options']);
    $request->record = $record;
    $request->requestId = $request_id;
    return $request;
  }

  /**
   * Is this a deferred request.
   *
   * @return bool
   *   TRUE if a deferred request.
   */
  public function isDeferred() {
    return $this->isDeferred;
  }

  /**
   * Is this a request with a callback
   *
   * @return mixed
   *   Returns callback
   */
  public function hasCallback() {
    return $this->callback;
  }

  /**
   * Is a response value set?
   *
   * @return bool
   *   TRUE if it has a response value
   */
  public function hasResponseValue() {
    return $this->responseValue;
  }

  /**
   * Does this request have an id?
   *
   * @return string
   *   Request Id or NULL if not.
   */
  public function getRequestId() {
    return $this->requestId;
  }

  /**
   * Parse options and set internal flags.
   */
  protected function handleOptions($options) {
    // Basic check - are the options a callback or options for a deferred call.
    if (is_array($options) && !is_callable($options)) {
      $this->options = $options;
      // Check whether they want to queue / is a dnodeq server running?
      $this->isDeferred = is_array($this->options) && (
        isset($this->options['requestQueue']) ||
        isset($this->options['responseQueue']) ||
        (isset($this->options['responseHttpCallback']) && $this->options['responseHttpCallback'])
      );
      $this->options['module'] = isset($this->options['module']) ? $this->options['module'] : 'dnode';
      $this->options['serverId'] = $this->serverId;
    }
    else {
      // @todo: can we do anything more meaningful here?
      $this->options = array();
      $this->callback = $options;
    }
  }

  /**
   * Create a persisted record for this request. Needed for deferred RPC with
   * callbacks.
   *
   * @return bool|stdClass
   *   Returns FALSE if a record could not be created or a stdClass representing
   *   the record.
   */
  public function createRequestRecord() {
    module_load_include('inc', 'dnode');
    $this->options['strategy'] = $this->strategy;
    $this->requestId = $this->options['request_id'] = dnode_generate_request_id();
    if (isset($this->options['responseHttpCallback']) && $this->options['responseHttpCallback'] === TRUE) {
      $this->options['responseHttpCallback'] = dnode_generate_callback_url($this->options['request_id'], $this->options['module']);
    }

    $request = dnode_create_request($this->serverId, $this->method, $this->arguments, $this->options);
    if (!is_object($request)) {
      return FALSE;
    }
    else {
      return $this->record = $request;
    }
  }

  /**
   * Return request options
   *
   * @return array
   *   Options
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Return request status
   *
   * @param bool $reset
   *   Reload the request record
   *
   * @return bool|null|int
   *   If a request id exists it checks for the status of the response
   *   Otherwise directly in the response value.
   */
  public function getResponseStatus($reset = TRUE) {
    if ($this->getRequestId()) {
      $this->loadRequestRecord($reset);
      if (!is_object($this->record)) {
        return NULL;
      }
      return $this->record->status;
    }
    elseif ($this->hasResponseValue()) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Helper function to set response values directly. Used by synchronous
   * calls (i.e. dnode_http).
   */
  public function setResponseValue($value, $error = FALSE) {
    if ($this->getRequestId()) {
      module_load_include('inc', 'dnode');
      dnode_update_response($this->getRequestId(), $value, $error ? DNODE_REQUEST_STATUS_ERROR : DNODE_REQUEST_STATUS_OK);
      $this->loadRequestRecord(TRUE);
    }
    else {
      $this->responseValue = $value;
    }
  }

  /**
   * Instatiate a DrupalDnodeResponse object.
   *
   * @return DrupalDnodeResponse
   *   Returns an instance of DrupalDnodeResponse
   */
  public function getResponseObject() {
    // Retrieve the newest from request record.
    if ($this->getRequestId()) {
      $this->loadRequestRecord(TRUE);
    }
    return $this->responseObject = new DrupalDnodeResponse($this->responseValue, $this);
  }

  /**
   * (Re)load request record.
   */
  protected function loadRequestRecord($reset = FALSE) {
    if ($reset || !isset($this->record)) {
      $this->record = (object) dnode_load_request(array('request_id' => $this->getRequestId()));
      if ($this->record) {
        $this->responseValue = unserialize($this->record->response);
      }
    }
    return $this->record;
  }
}
