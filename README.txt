dnode -  a drupal-nodejs integration module
===========================================

dnode is a popular and fun RPC protocol primarily used in nodejs projects.[1]

This modules provides a simple integration between dnode servers (in nodejs or
even in e.g php) and drupal.

You need to install some nodejs packages via npm! Please read INSTALL.txt for
installation instructions.

"STRATEGIES": dnode-php and http:
==================================
dnode.module is an API-module that doesn't do much by itself. It comes with two
"strategies" of communicating with
dnode servers - you will need to enable at least one of these two:

1) dnode_http uses a small nodejs server that accepts HTTP requests and passes
them on to dnode servers.
2) dnode_php uses the dnode-php[2] implementation of the dnode protocol in php.

dnode_http uses a (blocking) http request (in future httprl.module could be
used) - it has no special PHP requirements. dnode_php uses (non-blocking)
streams for communication with dnode-servers - it works with PHP5.3 only.
dnode_php moreover allows you have do more complicated communication patterns
(symmetric calling, multiple calls etc).

SERVER REGISTRY:
================
dnode provides a simple dnode-server registry (hook_dnode_info) that abstracts
the server from the port (or domain socket) the dnode-server listens on.
This also enables overriding/sharing configuration information across the
drupal-nodejs-boundary.
In future it will also provide "devop helpers", i.e. installing all required
npm dependencies, pings/monit, firewall etc ...

See hook_dnode_info().

CALLBACKS AND QUEUEING:
=======================
dnode also handles callbacks from nodejs. It comes with a more comfortable
dnodeq.module (requires: a *redis* server) that complements this functionality
by allowing you to:

- queue requests on the nodejs side using resque
- queue responses on the nodejs side and picking up them up when drupal sees fit

See dnode_rpc($server_id, $method, $arguments_as_array, $option_or_callback);

MESSAGE PASSING:
================
In addition to RPC/RMI dnode.module implements basic message passing which is
helpful when you don't require/need to wait for a result and or multiple modules
on the nodejs side should be able to react to the message (think: hooks).

See dnode_send_mesage($event, $data) for outgoing messages and
hook_dnode_message() for incoming messages.

EXTENSIBILITY AND DX:
=====================
One of the primary aims is to allow for allow for simple and rapid horizontal
extensibility allowing you to write small nodejs modules/servers that can safely
rest within your drupal codebase, integrate with your drupal configuration and
allow for rapid prototyping. By declaring dnode servers (or any kind of nodejs
server) via hook_dnode_info you can fire-up all servers with a simple drush
command:

$ drush dnode-nodejs-servers

EXAMPLES:
=========
There are a few examples of dnode-integration in the dnode_example module. For a
more interesting use-case you should  install the dnode_faye[3] sandbox which
integrates the wonderful faye[4] pubsub engine.

SECURITY:
=========
dnode normally works via TCP Ports - make sure to properly firewall your systems
in production!

[1] https://github.com/substack/dnode
[2] https://github.com/bergie/dnode-php
[3] http://drupal.org/sandbox/frega/1357240
[4] http://faye.jcoglan.com/
