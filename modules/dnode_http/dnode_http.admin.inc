<?php
/**
 * @file
 * Admin page callback file for the dnode_http module.
 */

/**
 * Form builder; Return form for dnode_http settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function dnode_http_settings() {
  $form = array();
  $form['dnode_http'] = array(
    '#type' => 'fieldset',
    '#title' => t('dnode_http - dnode rpc "proxied" via http.'),
  );
  $form['dnode_http']['dnode_http_server_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL to the dnode_http http-dnode server'),
    '#default_value' => variable_get('dnode_http_server_base_url', 'http://127.0.0.1:9998'),
    '#required' => FALSE,
  );
  $form['dnode_http']['dnode_http_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key/secret used to verify communication with server'),
    '#default_value' => variable_get('dnode_http_key', 'change me, really'),
    '#description' => t('Note: if you change this value, make sure to restart the servers.'),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}
