var express = require('express');
var qs = require('qs');
var Dnodeq = require('../../../../ssjs/lib/dnodeq/dnodeq').Dnodeq;

exports.DnodeHttpServer = {
  createDnodeHttpServer: function(config) {
    var debug = config.debug || false;
    var app = require('express').createServer();
    app.configure(function(){
       app.use(express.bodyParser());
       app.use(app.router);
    });

    var validateRequest = function(req) {
      if (typeof req.headers['x-dnode-http-key'] == 'undefined' || req.headers['x-dnode-http-key']  != config.key) {
        return false;
      }
      return true;
    }

    var dnodeq = new Dnodeq(config);
    app.post('/dnode/rpc', function(req, res, next) {
      // "authenticate" request signature
      if (!validateRequest(req)) {
        debug && console.log("dnode_http - invalid request");
        return res.send(403);
      }
      var socketPath = DrupalDnode.getDnodePortByServerId(req.body.serverId);
      debug && console.log("dnode_http", "serverId", req.body.serverId, "socketPath", socketPath, "body", req.body, "params", req.params, "method", req.body.method, "arguments", req.body.arguments, "options", req.body.options || {});
      if (!req.body.method || !req.body.arguments) {
        debug && console.log("dnode_http - invalid arguments");
        return res.send(500);
      }

      dnodeq.rpc(socketPath, req.body.method, req.body.arguments, req.body.options || {}, function(data) {
        debug && console.log("dnode_http - passed on to dnodeq - rpc-Arguments: ", socketPath, req.body.method, req.body.arguments, req.body.options || {}, "response", data);
        res.send(JSON.stringify(data));
      });
    });

    // Provide a ping route.
    app.post('/ping', function(req, res){
      res.send("pong");
    });

    // Fire-up the http server.
    var host = config.http_host || '127.0.0.1';
    app.listen(config.http_port, host);
    debug && console.log('Started dnode_http proxying server on http://' + host + ':' + config.http_port);
    return app;
  }
}
