var dnode = require('dnode');
DrupalDnode = global.DrupalDnode;

if (DrupalDnode) {
  var config = DrupalDnode.getModuleConfig('dnode_http');
} else {
  var config = require('./config.json');
}

// if we want HTTP based-access, fire it up
var app = require('./lib/dnodehttpserver.js').DnodeHttpServer.createDnodeHttpServer(config);
