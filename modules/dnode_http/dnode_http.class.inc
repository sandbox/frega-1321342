<?php

/**
 * @file
 * Class file for the dnode_http's DrupalDnodeHttp strategy.
 */

class DrupalDnodeHttp implements DrupalDnodeStrategyInterface {
  /**
   * Call a dnode server via http interface queue or unqueued (response or
   * request) including an optional http callback
   * on response.
   *
   * @param string $server_id
   *   ID of the dnode server @see hook_dnode_info().
   * @param string $method
   *   Method on the dnode server to call.
   * @param array $arguments
   *   Arguments passed to method as an array (à la call_user_func_array).
   * @param array $options
   *   An associative array containing following keys:
   *   - requestQueue: TRUE or string - dnode call will be queued (resque).
   *     Default queue is called 'dnodeq-requests'.
   *   - responseQueue: TRUE or string - response will be queued to allow for
   *     polling from Drupal @see dnode_http_get_responses().
   *     Cannot be combined with responseHttpCallback.
   *
   *   - responseHttpCallback: true or string - response will be returned via
   *     http callback from nodejs. If a string is provided it must be an
   *     absolute URL and handle by the caller. If true a URL will be generated
   *     and the response handled via hook_dnode_response() or if 'module' is
   *     set and {module}_dnode_response exists only by that implementation.
   *
   *
   *   - module: specify - optional.
   *
   *   - if no keys are given, a blocking HTTP request will be issued and the
   *     parsed response will *returned*
   *
   * @return mixed
   *   Result of rpc.
   **/
  function rpc($server_id, $method, $arguments, $options = array()) {
    $request = new DrupalDnodeRequest('dnode_http', $server_id, $method, $arguments, $options);
    if ($request->isDeferred()) {
      // Make sure we have a request_id and a persisted record.
      $request->createRequestRecord();
      $options = $request->getOptions();
    }
    // Build the HTTP-Request.
    $req = array(
      'method' => 'POST',
      'headers' => array(
        'X-dnode-http-key' => variable_get('dnode_http_key', 'change me, really'),
        'Content-Type' => 'application/json',
      ),
      'data' => drupal_json_encode(
        array(
          'serverId' => $server_id,
          'method' => $method,
          'arguments' => $arguments,
          'options' => is_array($options) ? $options : array(),
        )
      ),
    );
    // Issue request - note: this is blocking.
    $http_request = drupal_http_request(
      $url = variable_get('dnode_http_server_base_url', 'http://127.0.0.1:9998') . '/dnode/rpc',
      $req
    );
    // Let's log errors.
    if (isset($http_request->error)) {
      watchdog('dnode_http',
        'Error issueing http request to @url with @params',
        array('@url' => $url, '@params' => print_r($req, TRUE))
      );
    }
    // Add some specifics to the request object.
    // @todo: maybe subclass DrupalDnodeRequest.
    $request->http_request = $http_request;
    $request->http_error = isset($http_request->error) ? $http_request->error : NULL;
    $request->http_status = $http_request->code;
    // Add response - this is a synchronous call so we can set it.
    if (isset($http_request->data) && $http_request->data) {
      $request->setResponseValue(drupal_json_decode($http_request->data));
    }

    // If we have a Closure or are callback, use that (even though we're
    // synchronously calling, this might make sense).
    if ($request->hasCallback()) {
      // @todo - how do we handle errors? meh.
      $result = isset($http_request->data) ? drupal_json_decode($http_request->data) : array();
      call_user_func_array($request->callback, $result);
    }
    return $request;
  }

  /**
   * Call RPC (but with a call signature like call_user_func).
   *
   * @return mixed
   *   Result of rpc.
   */
  function rpcCallback() {
    $arguments = func_get_args();
    $server_id = array_shift($arguments);
    $method = array_shift($arguments);
    $callback = array_pop($arguments);
    return $this->rpc($server_id, $method, $arguments, $callback);
  }
}
