Drupal.behaviors.dnodeClientExample = {
  attach: function (context, settings) {
    jQuery('body').once(function() {
      var i = 0;
      Drupal.DNodeClient.connect(function(remote, conn) {

        setInterval(function(remote) {
          remote.dnodeExample(i, function(n) {
            jQuery('h1').text('Result from the other world: ' + n);
            i++;
          });
        }, 1000, remote);
      });
    });
  }
};
