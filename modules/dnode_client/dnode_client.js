Drupal.DNodeClient = {};
Drupal.DNodeClient.connect = function(fn, fn2) {
  // Check whether Dnode is available.
  if (typeof DNode === 'undefined') {
    // It would be nice, if Drupal had a guideline of properly dealing with
    // this kind of situation ...
    return false;
  }
  // Enable "bidirectional" communication.
  if (typeof fn2 !== 'undefined') {
    var client = DNode(fn);
    client.connect({
      port: Drupal.settings.dnode_client_socketio_port || 80,
      block: fn2
    });
  }
  else {
    DNode.connect({
      port: Drupal.settings.dnode_client_socketio_port || 80,
      block: fn
    });
  }
}
