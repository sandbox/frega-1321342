var _ = require('underscore');

// @todo: we should have a proper live-cycle for modules loading
// i.e. afterLoadingModules, beforeStartingModules, afterStartingModules
function setupDnodeClientModule() {
  if (DrupalDnode) {
    var config = DrupalDnode.getModuleConfig('dnode_client');
    var allConfig = DrupalDnode.configuration;
  } else {
    var config = require('./config.json');
    var dnodeModules = config.modules || {};
  }

  dnodeClient = {};
  dnodeClient.ping = function(n, cb) {
    return cb(n * 10);
  };

  _.forEach(allConfig, function(v, module) {
    if (v.dnode_client) {
      console.log('Module ' + module + '  exposes stuff - create a "proxy" ', v);
      _.forEach (v.dnode_client, function(authArgs, method) {
        dnodeClient[method] = function() {
          console.log('Method called', method, this.client);
          var args = Array.prototype.slice.call(arguments, 0);
          // Last argument is the callback.
          var cb = args.pop();
          DrupalDnode.connectByServerId(module, function(r, conn) {
            // Wrap the callback in a Handler.
            var cbHandler = function() {
              // Let's close this connection, to avod EMFILE issues.
              conn.end();
              // And ... callback.
              return cb.apply(cb, arguments);
            }
            // Add the callback handler as last argument.
            args.push(cbHandler);
            r[method].apply(r, args);
          })
        }
      });
    }
  });

  var express = require('express');
  var app = express.createServer();

  // then just pass the server app handle to .listen()!
  var dnode = require('dnode');
  var server = dnode(dnodeClient);

  server.listen(app);
  app.listen(config.port);
  console.log('dnode_client - started listening on port ' +  config.port + ' exposing methods: ', _.keys(dnodeClient));
}


process.nextTick(setupDnodeClientModule);
