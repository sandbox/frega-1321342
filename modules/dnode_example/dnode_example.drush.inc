<?php
/**
 * @file
 * Drush commands for dnode_example.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_example_drush_command() {
  $items['dnode-example-rpc'] = array(
    'callback' => 'dnode_example_dnode_example_rpc',
    'description' => 'Call dnode Example-methods via dnode-php',
    'arguments' => array(
      'users' => 'Optional value for rpc',
    ),
  );

  $items['dnode-example-test'] = array(
    'callback' => 'dnode_example_dnode_example_test',
    'description' => 'Run a few test',
  );

  $items['dnode-example-phpserver'] = array(
    'callback' => 'dnode_phpserver',
    'description' => 'Start an example dnode implemented in *PHP*',
  );
  return $items;
}

/**
 * Example dnode_php-rpc
 */
function dnode_example_dnode_example_rpc($value = FALSE) {
  $dnode = new DrupalDnodePhp();
  $example_things_from_outer_scope = ($value) ? (int) $value : 20;
  $dnode->connect(5052, function($remote, $connection) use ($example_things_from_outer_scope)  {
    drush_print('$dnode->connect - BEGIN');

    $remote->dnodeExample($example_things_from_outer_scope, function($r) use ($connection) {
      drush_print('$remote->dnodeExample(' . $example_things_from_outer_scope . ') ->: ' . $r);
    });
    $example_things_from_outer_scope = $example_things_from_outer_scope * 2;
    $remote->dnodeExample($example_things_from_outer_scope, function($r) use ($connection) {
      drush_print('$remote->dnodeExample(' . $example_things_from_outer_scope . ') ->: ' . $r);
    });
    $remote->dnodeExampleArray($example_things_from_outer_scope, function($r) use ($connection) {
      drush_print('$remote->dnodeExampleArray(' . $example_things_from_outer_scope . ') ->: ' . print_r($r, TRUE));
    });
    $remote->dnodeExampleObject($example_things_from_outer_scope, function($r) use ($connection) {
      drush_print('$remote->dnodeExampleObject(' . $example_things_from_outer_scope . ') ->: ' . print_r($r, TRUE));
      $connection->end();
    });
    drush_print('$dnode->connect - END');
  });
}

/**
 * Example dnode_php-rpc various tests.
 */
function dnode_example_dnode_example_test($value = FALSE) {
  $dnode = new DrupalDnodePhp();
  $example_things_from_outer_scope = ($value) ? (int) $value : 20;
  // Test various ways of connecting and RPCing.
  // 1) "raw" dnode style - specify port.
  $dnode->connect(5052, function($remote, $connection) use ($example_things_from_outer_scope)  {
    drush_print('$dnode->connect - BEGIN');
    $remote->dnodeExample($example_things_from_outer_scope, function($r) use ($connection) {
      drush_print('$remote->dnodeExample(' . $example_things_from_outer_scope . ') ->: ' . $r);
    });
    $connection->end();
  });

  // 2) use the dnode-registry (hook_dnode_info()).
  $dnode->connectByServerId('dnode_example', function($remote, $connection) {
    $remote->dnodeExample(20, function($r) use ($connection) {
      drush_print('$remote->dnodeExample(20) ->: ' . $r);
      $connection->end();
    });
  });
  // 3) rpcCallback call the dnode_faye service, method publish, arguments
  // and callback.
  $dnode->rpcCallback('dnode_example', 'dnodeExample', 20, 'dnode_example_dnode_example_test_callback');
  // 4) rpcIgnoreResponse - fire and forget.
  $dnode->rpcCallback('dnode_example', 'dnodeExample', 20, FALSE);

  // 5) rpcQueueResponse - fire and forget.
  $dnode->rpcCallback('dnode_example', 'dnodeExample', 20, FALSE);
}

/**
 * Example callback
 */
function dnode_example_dnode_example_test_callback($r) {
  drush_print('$dnode->rpcCallback() - dnode_example_dnode_example_test_callback -> ' . $r);
}

/**
 * Example dnode_php php-side server.
 */
function dnode_phpserver() {
  $dnode = new DrupalDnodePhp('DnodeExamplePhpServer');
  $dnode->listen(5052);
}

// This is the class we're exposing to DNode.
class DnodeExamplePhpServer {
  /**
   * Example function - multiplies by 10.
   *
   * @param int $n
   *   value to multiply times 10.
   * @param callback $cb
   *   callback / closure - returns the value as int.
   *
   * @return null
   *   this function does not return, it uses a callback.
   */
  public function dnodeExample($n, $cb) {
    $cb(10 * $n);
  }

  /**
   * Example function - multiplies by 10.
   *
   * @param int $n
   *   value to multiply times 10.
   * @param callback $cb
   *   callback / closure - returns the value as an array.
   *
   * @return null
   *   this function does not return, it uses a callback.
   */
  public function dnodeExampleArray($n, $cb) {
    $cb(array(10 * $n));
  }

  /**
   * Example function - multiplies by 10.
   *
   * @param int $n
   *   value to multiply times 10.
   * @param callback $cb
   *   callback / closure - returns the value as an object.
   *
   * @return null
   *   this function does not return, it uses a callback.
   */
  public function dnodeExampleObject($n, $cb) {
    $cb((object) array('result' => 10 * $n));
  }
}
