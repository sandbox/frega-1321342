/* very basic  */
exports.dnodeExample = function(a, cb) {
  console.log('dnodeExample: multiply ' + a + ' by 100 = ' + (a * 100));
  cb(a * 100);
}

exports.dnodeExampleArray = function(a, cb) {
  console.log('dnodeExampleArray: returns array(result); multiply ' + a + ' by 100 = ' + (a * 100));
  cb([a * 100]);
}

exports.dnodeExampleObject = function(a, cb) {
  console.log('dnodeExampleArray: returns object {result: XX};  multiply ' + a + ' by 100 = ' + (a * 100));
  cb({result: a * 100});
}

exports.verySlowFunction = function(a, cb) {
  console.log('dnodeExample::verySlowFunction - start delay 15seconds');
  setTimeout(function(cb,a) {
    console.log('dnodeExample::verySlowFunction - end delay 15seconds');
    cb(a * 100);
  }, 15000, cb, a);
}


// expose configuration
exports.config = {
  serverId: 'dnode_example',
  port: 5052
};
