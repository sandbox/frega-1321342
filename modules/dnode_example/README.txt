This module contains some sample implementations of how to use dnode.module.

0) enable dnode and dnode_example

drush en -y dnode dnode_example

======
FROM PHP -> NODEJS
======

1) start the nodejs server(s)

drush dnode-nodejs-servers

2) in a new terminal window, test the example php client via drush

drush dnode-example-rpc

The script calls the nodejs-example server implementation
(ssjs/dnode_example.dnode.js).


======
FROM PHP -> PHP
======

1) stop the nodejs server and start the php dnode server (for the lulz)

drush dnode-example-phpserver

2) in a new terminal run the *sample* example php client via drush (s. above)

drush dnode-example-rpc

This script now calls the php-example-dnode dnode implementation (*php*)

NOTE: PHP (and Drupal) is probably not a super-sweet solution for daemonized
thingies, just sayin' ...

=====
FROM JS -> JS (or PHP)
=====
Checkout ssjs/dnode_example_client.js.

=======
todo: FROM PHP -> NODEJS -> PHP - roundtrippin'
=======

Check out the implementation in bergie's dnode-class:

https://github.com/bergie/dnode-php/tree/master/examples/bidirectional
