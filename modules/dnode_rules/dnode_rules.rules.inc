<?php
/**
 * @file
 * rules definitions for dnode_rules.
 */

/**
 * Implements hook_rules_event_info().
 *
 * This function declares all dnode_rules_rules actions
 */
function dnode_rules_rules_action_info() {
  // we're only implementing actions for the node entity-type
  return array(
    'dnode_rules_dnode_call' => array(
      'label' => t('dnode_rules: Call an dnode method'),
      'parameter' => array(
        'server_method' => array(
          'type' => 'text',
          'label' => t('dnode server and method to call'),
          'optional' => FALSE,
          'options list' => 'dnode_rules_rules_action_info_server_options',
        ),
        'arguments' => array(
          'type' => 'text',
          'label' => t('Arguments passed onto the method (JSON-encoded array, please)'),
          'optional' => TRUE,
        ),
        'callback' => array(
          'type' => 'text',
          'label' => t('PHP function name to handle the callback (optional)'),
          'optional' => TRUE,
        ),
      ),
      'group' => t('dnode_rules'),
      'base' => 'dnode_rules_call',
      'callbacks' => array(),
    ),
  );
}

/**
 * Call a dnode method.
 */
function dnode_rules_call($server_method, $arguments = '', $cb = NULL, $element = NULL) {
  list($server, $method) = explode('::', $server_method);
  if (!is_array($arguments)) {
    if (!empty($arguments)) {
      $arguments = drupal_json_decode($arguments);
    }
    else {
      $arguments = array();
    }
  }
  $dnode_info = dnode_get_dnode_info();
  $server_info = isset($dnode_info[$server]) ? $dnode_info[$server] : FALSE;
  if (!$server_info) {
    watchdog('dnode_rules', "Invalid call; no information on dnode-server @server found.",
      array('@server' => $server)
    );
    return FALSE;
  }
  try {
    $dnode = dnode_get_dnode_instance(array('strategy' => 'dnode_php'));
    dnode_rpc($server, $method, $arguments, array('strategy' => 'dnode_php', 'callback' => $cb));
  } catch (Exception $e) {
    watchdog('dnode_rules', 'Exception on connecting' . $e->getMessage());
  }
}

/**
 * Retrieve all callable dnode-methods.
 */
function dnode_rules_rules_action_info_server_options($element = NULL) {
  if (module_exists('dnode_php')) {
    $dnode_info = dnode_php_get_dnode_methods();
    $options = array();
    foreach ($dnode_info as $id => $info) {
      foreach ($info as $method) {
        $options[$id . '::' . $method] = $id . '::' . $method;
      }
    }
    return $options;
  }
}
