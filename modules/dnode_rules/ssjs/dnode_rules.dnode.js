var dnode = require('dnode');
if (DrupalDnode) {
  var config = DrupalDnode.getModuleConfig('dnode_rules');
} else {
  var config = require('./config.json');
}

exports.triggerComponent = function(component, args, options, cb) {
  if (cb==null) {
    cb = options;
    options = {};
  }
  DrupalDnode.connectByServerId('dnodeq', function(r, c) {
    r.sendMessageToDrupal(
      'dnode_rules',
      {'component': component, 'arguments': args},
      options,
      function(err, data) {
        c.end();
        cb(err, data);
      }
    );
  })
};
