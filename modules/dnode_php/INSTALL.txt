INSTALLING
==========

Note: You will need PHP5.3 for this module.

Composer/packagist is used to provide the dnode-php dependency.

1) Change into the dnode_php-module directory:

cd sites/default/modules/dnode/modules/dnode_php/

Note: this can differ in your setup. Change the path accordingly.

2) Then let composer handle the dependencies:

curl -s http://getcomposer.org/installer | php
php composer.phar install
