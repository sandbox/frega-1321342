<?php
/**
 * @file
 * Drush commands for dnode_php
 */

/**
 * Implements hook_drush_command().
 */
function dnode_php_drush_command() {
  $items['dnode-php-server'] = array(
    'callback' => 'dnode_php_server',
    'description' => 'Start dnode servers (php)',
  );
  return $items;
}

/**
 * Start dnode php servers.
 */
function dnode_php_server($server_id) {
  _dnode_php_bootstrap();
  $modules = module_implements('dnode_info');
  $dnodes = array();
  foreach ($modules as $m) {
    $fn = $m . '_dnode_server_info';
    $sv = $fn();
    foreach ($sv as $_server_id => $server_info) {
      if ($server_info['type'] == 'php' && $server_id == $_server_id) {
        $dnodes[$server_id] = new DrupalDnodePhp($server_info['class']);
        drush_print("Starting php server dnode: $server_id ({$server_info['class']}:{$server_info['port']}");
        $dnodes[$server_id]->listen($server_info['port']);
      }
    }
  }
}
