<?php
/**
 * @file
 * DrupalDnodePhp a dnode-php based strategy to communicate with dnode-servers.
 */

/**
 * Composer autoloader
 */
require __DIR__.'/vendor/autoload.php';



use DNode\DNode;
class DrupalDnodePhp extends DNode implements DrupalDnodeStrategyInterface {
  /**
   * Constructor
   *
   * @param array $options
   *   Options passed to constructor
   */
  public function __construct($options = array()) {
    $wrapper_class = NULL;
    if (is_array($options)) {
      $wrapper_class = isset($options['class']) ? $options['class'] : NULL;
    }
    elseif (is_string($options)) {
      $wrapper_class = $options;
    }
    return parent::__construct($wrapper_class);
  }
  /**
   * Connect to a dnode server in the Dnode.module registry (hook_dnode_info).
   *
   * @param string|int $id
   *   ID of a dnode-server in the registry or a port (if numeric)
   * @param Closure $closure
   *   function($remote, $connection) {} must call $connection->end();
   *
   * @return void
   *   This function does not return instead the Closure must call end on
   *   $connection.
   *
   * @throws Exception
   */
  public function connectByServerId($id, Closure $closure) {
    if (is_numeric($id)) {
      $params = array('port' => $id);
    }
    else {
      $info = dnode_get_dnode_info();
      $params = isset($info[$id]) ? $info[$id] : FALSE;
      if (!$params) {
        throw new \Exception("Invalid Dnode Server Id given");
        return ;
      }
    }

    $params['block'] = $closure;
    // @todo: let's test Unix domain socket - "should" work.
    if (isset($params['path'])) {
      $socket = $params['path'];
    }
    else {
      if (!isset($params['host'])) {
          $params['host'] = '127.0.0.1';
      }
      if (!isset($params['port'])) {
          throw new \Exception("For now we only support TCP connections to a defined port");
      }
      // @todo: Websockets?
      $socket = "tcp://{$params['host']}:{$params['port']}";
    }

    $stream = stream_socket_client($socket);
    if (!$stream) {
        throw new \Exception("No connection to DNode server at/on " . $socket);
    }
    $this->handleConnection($stream, $params);
  }

  /**
   * Call a single RPC function.
   *
   * @param string $server_id
   *   ID or port of the service
   * @param string $method
   *   method to call
   * @param array $arguments
   *   array of arguments
   * @param array $options
   *   Options
   *   - string|callback|Closure: 'exampleFunction', array('StaticClass',
   *     'method'), array($object, 'method')
   *   - any other arguments will be routed through hook 'dnode_rpc_response'
   *   - FALSE - no callback
   *
   * @return mixed
   *   Returns depends on options
   */
  public function rpc($server_id, $method, $arguments, $options = array()) {
    $request = new DrupalDnodeRequest('dnode_php', $server_id, $method, $arguments, $options);
    if ($request->isDeferred()) {
      if (!module_exists('dnodeq')) {
        watchdog('dnode_php', 'rpc called with requestQueue or responseQueue or responseHttpCallback - available only via dnodeq. Enable dnodeq.module', array(), WATCHDOG_ERROR);
      }
      else {
        // Create a request_record.
        $request_record = $request->createRequestRecord();
        // Let's do a switcheroo and execute it via the dnodeq service.
        $arguments = array(
          $server_id,
          $method,
          $arguments,
          $request->getOptions(),
        );
        $server_id = 'dnodeq';
        $method = 'rpc';
      }
    }

    $this->connectByServerId($server_id, function($remote, $connection) use ($request, $server_id, $method, $arguments, $options) {
      $cb = function() use ($connection, $request, $server_id, $method, $arguments, $options) {
        // @todo: are the situations in which it makes sense to keep the
        // connection alive / pass it on?
        $connection->end();
        // If $options is not true-ish, let's ignore the response.
        if ($options) {
          $response_args = func_get_args();
          if ($request->hasCallback()) {
            call_user_func_array($request->callback, $response_args);
          }
          elseif (!$request->isDeferred()) {
            // Wrap the response.
            $response = new DrupalDnodeResponse($response_args, $request);
            dnode_invoke_response($request, $response);
          }
        }
      };
      // Push the closure onto the arguments array.
      $arguments[] = $cb;
      call_user_func_array(array($remote, $method), $arguments);
    });
    return $request;
  }

  /**
   * Call RPC (but with a call signature like call_user_func).
   *
   * @return mixed
   *   Result of rpc.
   */
  public function rpcCallback() {
    $arguments = func_get_args();
    $server_id = array_shift($arguments);
    $method = array_shift($arguments);
    $callback = array_pop($arguments);
    return $this->rpc($server_id, $method, $arguments, $callback);
  }
}
