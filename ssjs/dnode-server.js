var argv = require('optimist').argv;
var _ = require('underscore');

var options = {};
// port for the dnode default server, if needed
options.port = options.port || 5050;
options.debug = options.debug || false;

// @todo: is there a better way, than using global scope?
global.DrupalDnode = require('./lib/drupaldnode').DrupalDnode;

// simple option injection
_.extend(options, argv);

// let's parse modules (assume we get a JSON array).
var modules = JSON.parse(options.modules);
options.config = options.config ? JSON.parse(options.config) : {};

// basic validation
if (!options.root) {
  console.error('Please provide a --root option that corresponds to the Drupal project root.')
}
if (!options.modules) {
  console.error('Please provide a --module option. It should be a JSON-encoded string {"moduleName": "modulePath", ...}.');
}

// load the modules and the configuration
DrupalDnode.requireModules(modules, options);
// start the servers
DrupalDnode.startDnodeServers(options);

