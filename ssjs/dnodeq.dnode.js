var _ = require('underscore');

var Dnodeq = require('./lib/dnodeq/dnodeq').Dnodeq;
// if we want to enable the dnode_http/dnodeq be available via dnode

if (DrupalDnode) {
  var config = DrupalDnode.getModuleConfig('dnodeq');
} else {
  var config = require('./config.json');
}

var dnodeq = new Dnodeq(config);
DnodeqDnodeServer = {
  /**
   * Issue a RPC via dnode
   * @param serverId
   * @param method
   * @param args
   * @param options
   * @param cb
   */
  'rpc': function(serverId, method, args, options, cb) {
    var dnodePort = DrupalDnode.getDnodePortByServerId(serverId);
    options.serverId = options.serverId || serverId;
    if (options.queueRequest) {
      dnodeq.enqueueRpc(dnodePort, method, args, options);
      return cb();
    } else {
      dnodeq.rpc(dnodePort, method, args, options, cb);
    }
  },
  /**
   * Issue a queued RPC via dnode
   * @param serverId
   * @param method
   * @param args
   * @param options
   * @param cb
   */
  'queuedRpc': function(serverId, method, args, options, cb) {
    options.queueRequest = true;
    return this.rpc(serverId, method, args, options, cb);
  },
  /**
   * Emit a message
   * @param event
   * @param data
   */
  'emitMessage': function(event, data, cb) {
    DrupalDnode.emitter.emit(event, data);
    cb();
  },
  /**
   * Issue a queued RPC via dnode
   * @param module
   * @param args
   * @param options
   * @param cb
   */
  'sendMessageToDrupal': function(module, data, options, cb) {
    options.headers = options.headers || {};
    options.headers['X-DNODE-SERVICE-KEY'] = config.dnode_service_key;
    dnodeq.httpCallback(config.drupal_base_url + '/dnode/message/' + module, data, options, cb);
  },

  /**
   * Enqueue items
   * @param string queue
   * @param mixed payload
   * @param callback
   */
  'enqueue': function(queue, payload, callback) {
    dnodeq.enqueue(queue, payload, callback);
  },
  /**
   * Dequeue/get
   * @param string queue
   * @param mixed payload
   * @param callback
   */
  'dequeue': function(queue, limit, callback) {
    dnodeq.dequeue(queue, limit, callback);
  },
  /**
   * Get responses from response queue
   * @param options
   *   - queue: optional name of the queue to check
   *   - limit: maximum no. of items to dequeue
   * @param cb
   *   callback
   */
  'getRpcResponses': function(options, cb) {
    this.dequeue(options.queue || 'dnode-responses', options.limit || 10, cb);
  },
  /**
   * Echo function for testing purposes.
   * @param a
   *   value to multiply times 10.
   * @param cb
   *   callback
   */
  'echoTimesTen': function(a, cb) {
    return cb(a * 10);
  },
  /**
   * Delayed echo function for testing purposes.
   * @param a
   *   value to multiply times 10.
   * @param ts
   *   time to delay response by in ms.
   * @param cb
   *   callback
   */
  'echoTimesTenDelayed': function(a, ts, cb) {
    setTimeout(function(a, cb) {
      return cb(a * 10);
    }, ts, a, cb);
  }
}

// make this also available via dnode, i say: why not?
_.extend(exports, DnodeqDnodeServer);

exports.config = {
  serverId: config.dnodeqServer.serverId,
  port: config.dnodeqServer.port
};
