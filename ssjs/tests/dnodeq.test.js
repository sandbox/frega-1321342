var
  mocha = require('mocha'),
  assert = require('assert'),
  util = require('util');
var _ = require('underscore');


var Dnodeq = require('../lib/dnodeq.js').Dnodeq;
var config = require('../../../../ssjs/config.json');
var dnodeq = new Dnodeq(config);

// set-up a simple mock dnode server
var dnode = require('dnode');
dnodeServer = {
  timesTen: function(r, cb) {
    cb(null, r * 10);
  }
}
var s = dnode(dnodeServer);
s.listen(11111);

describe('dnodeq - rpc (unqueued/queued requests, unqueued/queued responses)', function(){
  describe('initial mock-test dnode ', function(){
    it('timesTen(5) via dnode is 50.', function(done){
      var self = this;
      dnode.connect(11111, function(remote) {
        remote.timesTen(5, function(err, data) {
          assert.equal(data, 50);
          done();
        });
      });
    });
    it('retrieving queued responses should return nothing', function(done){
      dnodeq.getRpcResponses({'queue': 'testing-queue', limit: 1000}, function(err, data) {
        // empty it
        dnodeq.getRpcResponses({'queue': 'testing-queue', limit: 1000}, function(err, data) {
          assert.equal(data.length, 0);
          done(err, data);
        });
      });
    });
  });
  describe('dnodeq - immediate', function(){
    dnodeq.rpc(11111, 'timesTen', [5], {}, function (data) {
      // there should be one in the queued
      assert.equal(data.arguments[1], 50);
    });
    dnodeq.rpc(11111, 'timesTen', [5], {'responseValueMap': ['err', 'value']}, function (data) {
      // there should be one in the queued
      assert.equal(data.arguments[1], 50);
    });
  });
  describe('dnodeq - queuedResponse', function(){
    it('dnodeq - responseQueue timesTen(5) should leave one item in the queue and the response should 50', function(done) {
      dnodeq.rpc(11111, 'timesTen', [5], {responseQueue: 'testing-queue'},
        function (err, data) {
          // there should be one in the queued
          assert.equal(data, 1);
          // reset the queue
          dnodeq.getRpcResponses({'queue': 'testing-queue', limit: 100}, function(err, data) {
            assert.equal(data.length, 1);
            assert.equal(data[0].response.arguments[1], 50);
            done(err, data);
          });
        }
      );
    });
  });
  describe('dnodeq - httpCallbacks', function(){
    var server;
    beforeEach(function(done) {
      var http = require('http');
      server = http.createServer(function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Hello World\n');
      }).listen(1337, "127.0.0.1");
      console.log('Server running at http://127.0.0.1:1337/');
      done();
    });
    it('dnodeq - httpCallback should immediately return the arguments and callback the server', function(done) {
      server.on('request', function(req, res) {
        var body = '';
        console.log('request incoming!');
        req.on('data', function(chunk) { body += chunk; });
        req.on('end', function() {
          var p = JSON.parse(body);
          assert.equal(p.arguments[1], 50);
          done();
        });
      });
      dnodeq.rpc(11111, 'timesTen', [5], {responseHttpCallback: 'http://127.0.0.1:1337/test123'},
        function (err, data) {
          assert.equal(data.arguments[1], 50);
        }
      );
    });
    afterEach(function(done) {
      console.log('close server');
      server.close();
      done();
    });
  });
});

