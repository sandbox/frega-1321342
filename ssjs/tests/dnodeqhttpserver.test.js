var
  mocha = require('mocha'),
  assert = require('assert'),
  http = require('http'),
  url = require('url'),
  qs = require('qs'),
  util = require('util');


var DnodeqHttpServer = require('../lib/dnodehttpserver.js').DnodeqHttpServer;
var config = require('../../../../ssjs/config.json');

// set-up a simple mock dnode server
var dnode = require('dnode');
dnodeServer = {
  timesTen: function(r, cb) {
    cb(null, r * 10);
  }
}
var s = dnode(dnodeServer);
s.listen(11111);

global.DrupalDnode = {
  getDnodePortByServerId: function(id) {
    return id;
  }
}


function _postRequest(urlParam, payload, type, cb) {
  if (typeof urlParam == 'object') {
    var opts = urlParam;
  } else {
    // we assume it's just a URL
    var opts = url.parse(urlParam);
  }

  var debug = false;
  // build the response body, mixin the options
  var payload = payload || {};

  if (type=='json') {
    var postData = JSON.stringify(payload);
  } else {
    var postData = qs.stringify(payload);
  }
  // this request can be lengthy and is probably never idempontent; so we force POST.
  opts.method = 'POST';
  opts.headers = opts.headers || {};
  if (type=='json') {
    opts.headers['Content-Type'] = 'application/json';
  } else {
    opts.headers['Content-Type'] = 'application/x-www-form-urlencoded';
  }
  opts.headers['Content-Length'] = postData.length;
  opts.headers['X-dnodeq-http-key'] =
    typeof opts.headers['X-dnodeq-http-key'] != 'undefined' ?
      opts.headers['X-dnodeq-http-key'] : config.httpServer.key;

  var req = http.request(opts, function(res) {
    var responseBody = '';
    // @todo - do we care about this?
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      responseBody += chunk;
    });
    res.on('end', function() {
      debug && console.log('_httpCallback completed - ' + responseBody);
      cb(null, res.statusCode, responseBody);
    });
  });
  console.log('postRequest', opts.path, postData);
  req.write(postData);
  req.on('error', function(e) {
    debug && console.log('problem with request: ' + e.message);
  });
  req.end();

}

function postRequest(urlParam, payload, cb) {
  _postRequest(urlParam, payload, 'json', cb);
}


describe('dnodeqhttpserver', function(){
  var app;
  before( function(done) {
    app = DnodeqHttpServer.createDnodeqHttpServer(config);
    done();
  });
  it('timesTen(5) via dnode is 50.', function(done){
    var self = this;
    dnode.connect(11111, function(remote) {
      remote.timesTen(5, function(err, data) {
        assert.equal(data, 50);
        done();
      });
    });
  });
  it('/test should return 200', function(done){
    postRequest('http://127.0.0.1:9998/ping', {'serverId': 11111, 'method': 'timesTen', 'arguments': [5], options: {}}, function(err, statusCode, responseBody) {
      assert.equal(responseBody, 'pong');
      assert.equal(statusCode, 200);
      done();
    });
  });

  it('invalid request should return 500', function(done){
    postRequest('http://127.0.0.1:9998/dnodeq/rpc', {}, function(err, statusCode, responseBody) {
      assert.equal(statusCode, 500);
      done();
    });
  });

  it('request with invalid X-dnodeq-http-key header should 403', function(done){
    var opts = url.parse('http://127.0.0.1:9998/dnodeq/rpc');
    opts.headers = {};
    opts.headers['X-dnodeq-http-key'] = 'XXXX';
    postRequest(opts, {'serverId': 11111, 'method': 'timesTen', 'arguments': [5]}, function(err, statusCode, responseBody) {
      assert.equal(statusCode, 403);
      done();
    });
  });




  it('immediate request should return 50', function(done){
    postRequest('http://127.0.0.1:9998/dnodeq/rpc', {'serverId': 11111, 'method': 'timesTen', 'arguments': [5]}, function(err, statusCode, responseBody) {
      assert.equal(statusCode, 200);
      var response = JSON.parse(responseBody);
      assert.equal(response.arguments[1], 50);
      done();
    });
  });
  it('immediate request should return 50', function(done){
    postRequest('http://127.0.0.1:9998/dnodeq/rpc', {
      'serverId': 11111, 'method': 'timesTen', 'arguments': [5], 'options': {'responseQueue': 'testing-queue'}
    }, function(err, data) {
      assert.equal(err, null);
      postRequest('http://127.0.0.1:9998/dnodeq/getRpcResponses', {'options': {'queue': 'testing-queue', 'limit': 100}}, function(err, statusCode, responseBody) {
        var response = JSON.parse(responseBody);
        assert.equal(response[0].response.arguments[1], 50);
        done();
      });
    });
  });

  after(function(done) {
    app.close();
    done();
  });
});