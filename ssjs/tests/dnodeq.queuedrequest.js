var mocha = require('mocha'),
    assert = require('assert'),
  util = require('util');

var Dnodeq = require('../lib/dnodeq.js').Dnodeq;
var config = require('../../../../ssjs/config.json');
var dnodeq = new Dnodeq(config);

// set-up a simple mock dnode server
var dnode = require('dnode');
dnodeServer = {
  timesTen: function(r, cb) {
    console.log('timesTen called', r, r *10 );
    cb(null, r * 10);
  }
}
var s = dnode(dnodeServer);
s.listen(11111);

dnodeq.enqueueRpc(11111, 'timesTen', [5], {responseQueue: 'testing-queue'});
dnodeq.enqueueRpc(11111, 'timesTen', [5], {responseHttpCallback: 'http://127.0.0.1:1337/test123'});
var server;
var http = require('http');
server = http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
}).listen(1337, "127.0.0.1");
console.log('Server running at http://127.0.0.1:1337/');

server.on('request', function(req, res) {
  var body = '';
  console.log('request incoming!');
  req.on('data', function(chunk) { body += chunk; });
  req.on('end', function() {
    var p = JSON.parse(body);
    console.log('Request body', body, p);
    assert.equal(p.arguments[1], 50);
    server.close();
  });
});

setTimeout(function() {
  dnodeq.getRpcResponses({'queue': 'testing-queue', limit: 100}, function(err, data) {
    console.log(data);
    assert.equal(data.length, 1);
    assert.equal(data[0].arguments[1], 50);
  });
});
