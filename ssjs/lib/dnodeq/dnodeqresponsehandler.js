var Queuemanager = require('./queuemanager').Queuemanager;

var DnodeqResponseHandler = function(config) {
  this.config = config || {};
  this.debug  = config.debug || false;
  this.queues = {};
}

DnodeqResponseHandler.prototype.getQueue= function(queueId) {
  if (typeof this.queues[queueId] == 'undefined') {
    // @todo: switch by queueId? dependency injection argh.
    this.queues[queueId] = Queuemanager.createQueue(queueId, this.config.queueType || 'memory', this.config);
  }
  return this.queues[queueId];
}

DnodeqResponseHandler.prototype.enqueue = function(queueId, data, cb) {
  this.getQueue(queueId).enqueue(data, cb);
}

DnodeqResponseHandler.prototype.dequeue = function(queueId, limit, cb) {
  this.getQueue(queueId).dequeue(limit, cb);
}

exports.DnodeqResponseHandler = DnodeqResponseHandler;