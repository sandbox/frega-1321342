var Queue = function(options) {
  this.q = [];
}

Queue.prototype.enqueue = function(data, cb) {
  this.q.push(data);
  cb(null, this.q.length);
}

Queue.prototype.dequeue = function(limit, cb) {
  cb(null, this.q.splice(0, limit));
}

Queue.prototype.length = function(cb) {
  cb(null, this.q.length);
}

exports.Queue = Queue;
