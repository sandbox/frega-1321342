var dnode = require('dnode');
var resque = require('coffee-resque');
var util = require('util');

var DnodeqResponseHandler = require('./dnodeqresponsehandler.js').DnodeqResponseHandler;
var DnodeqRequestResqueHandler = require('./dnodeqresquehandler.js').DnodeqRequestResqueHandler;
var DnodeHttpCallback = require('./dnodehttpcallback.js').DnodeHttpCallback;

/**
 * Helper function that invokes a remote method
 *
 * @param dnodePort
 * @param method
 * @param methodArgs (arguments as a array);
 * @param cb callback
 * @return void
 */
function dnodeRpc(dnodePort, method, methodArgs, cb) {
  methodArgs.push(cb);
  try {
    dnode.connect(dnodePort, function(remote, conn) {
      if (typeof remote[method] == 'undefined') {
        return cb(new Error('invalid method'));
      }
      remote[method].apply(this, methodArgs);
      conn.end();
    });
  } catch (e) {
    console.log('dnodeRpc error', e);
    cb(e, null);
  }
}

/**
 * Handles queued rpc calls.
 */
var DnodeqRequestResqueJobHandler = {
  rpc: function(dnodePort, method, methodArgs, options, cb) {
    dnodeRpc(dnodePort, method, methodArgs, function() {
      var args = Array.prototype.slice.call(arguments, 0);
      cb(args);
    });
  }
}

/**
 * Dnodeq constructor.
 */
exports.Dnodeq = Dnodeq = function(config) {
  this.config = config;
  if (this.config.responseQueue && this.config.responseQueue.enabled) {
    this.dnodeqResponseHandler = new DnodeqResponseHandler(this.config.responseQueue);
  }
  if (this.config.requestQueue && this.config.requestQueue.enabled) {
    this.dnodeqRequestResqueHandler =
      new DnodeqRequestResqueHandler(this, DnodeqRequestResqueJobHandler, this.config.requestQueue.resqueConfig);
    this.dnodeqRequestResqueHandler.worker.start();
  }
};

var methods = {
  /**
   * Enqueue a request to be executed in a delayed fashion
   * @param dnodePort
   * @param method
   * @param methodArgs
   * @param options
   */
  enqueueRpc: function(dnodePort, method, methodArgs, options) {
    return resque.connect().enqueue('dnodeq-request', 'rpc', [dnodePort, method, methodArgs, options]);
  },
  // }}}
  /**
   * Dnode rpc -
   * @param dnodePort
   * @param method
   * @param methodArgs
   * @param options
   *   Optional:
   *   - requestQueue - queue the request instead of executing it immediately.
   *
   *   Either:
   *   - responseQueue - queue a response
   *   or:
   *   - responseHttpCallback - send response directly via HTTP to drupal
   *
   * @param cb
   *   callback function
   *   NOTE: in order to harmonize responses this function *always* returns response in this matter
   *
   *
   */
  rpc: function(dnodePort, method, methodArgs, options, cb) {
    this.config.debug && console.log('dnodeq - incoming - dnodePort', dnodePort, 'method' , method, 'methodArgs', methodArgs, 'options', options);
    if (options.requestQueue) {
      this.enqueueRpc(dnodePort, method, methodArgs, options);
      return cb();
    }
    var self = this;
    dnodeRpc(dnodePort, method, methodArgs, function() {
      var args = Array.prototype.slice.call(arguments, 0);
      self.config.debug && console.log('dnodeRpc - responseHttpCallback:', args);
      if (options.responseQueue) {
        // we queue the response
        self.enqueue(options.responseQueue, {
          'request': {
            'serverId': options.serverId ? options.serverId : null,
            'dnodePort': dnodePort,
            'method': method,
            'arguments': methodArgs
          },
          'response': args,
          'options': options
        }, cb);
      } else if (options.responseHttpCallback) {
        self.config.debug && console.log('dnodeq - responseHttpCallback:', args);
        self.httpCallback(options.responseHttpCallback, args, options, cb);
      } else {
        self.config.debug && console.log('dnodeq - returns:', args);
        cb(args);
      }
    });
  },

  // }}}
  // {{{ queued *RESPONSE*
  enqueue: function(queueId, payload, callback) {
    if (!queueId || queueId===true) {
      queueId = 'dnodeq-response';
    }
    this.dnodeqResponseHandler.enqueue(queueId, payload, callback);
  },
  dequeue: function(queueId, limit, cb) {
    this.dnodeqResponseHandler.dequeue(queueId, limit, cb);
  },
  // }}}
  // {{{ httpCallback
  httpCallback: function(urlParam, payload, options, cb) {
    return new DnodeHttpCallback(this.config, urlParam, payload, options, cb);
  }
}

for (var m in methods) {
  Dnodeq.prototype[m] = methods[m];
}
