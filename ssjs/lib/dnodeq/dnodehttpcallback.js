var http = require('http');
var url = require('url');
var qs = require('qs');

var debug = false;

exports.DnodeHttpCallback = function(config, urlParam, payload, options, cb) {
  if (typeof urlParam == 'object') {
    var opts = urlParam;
  } else {
    // we assume it's just a URL
    var opts = url.parse(urlParam);
  }

  debug && console.log('httpCallback', urlParam, payload, options, opts);
  // Build the response body, mixin the options.
  var payload = payload || {};
  var postData = JSON.stringify(payload);

  // This request can be lengthy and is probably never idempotent; so we force POST.
  opts.method = 'POST';
  opts.headers = options.headers || {};
  opts.headers['Content-Type'] = 'application/json';
  opts.headers['Content-Length'] = postData.length;
  debug && console.log(opts);
  var req = http.request(opts, function(res) {
    var responseBody = '';
    // @todo - do we care about this?
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      responseBody += chunk;
    });
    res.on('end', function() {
      debug && console.log('_httpCallback completed');
    });
  });
  req.write(postData);
  req.on('error', function(e) {
    debug && console.log('problem with request: ' + e.message);
  });
  req.end();
  // @todo: one could argue for putting the call elsewhere (e.g res.on('end');
  return cb(null, payload);
}