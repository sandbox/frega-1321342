exports.DnodeqRequestResqueHandler = DnodeqRequestResqueWorker = function(dnodeq, jobHandler, config) {
  this.config = config;
  this.debug = config.debug || false;
  var self = this;

  var handleEvent = function(type, worker, queue, job, payload) {
    // options are the last argument to the rpc call
    var options = job.args ? job.args.pop() : {};
    options = (typeof options == 'object') ? options : {};
    if (options.responseHttpCallback) {

      dnodeq.httpCallback(options.responseHttpCallback, payload, options, function(err, data) {
        self.debug && console.log('dnodeRequestResqueWorker httpCallback');
      });
    } else {
      options.responseQueue = ( options.responseQueue === true ) ? 'dnodeq-responses' : options.responseQueue;
      dnodeq.enqueueResponse(options.responseQueue, {job: job, options: options, result: payload}, function() {});
    }
  }

  this.worker = require('coffee-resque').connect(config).worker('dnodeq-request', jobHandler);
  this.worker.on('error', function(err, worker, queue, job) {
    handleEvent('error', worker, queue, job, err);
  });
  this.worker.on('success',  function(worker, queue, job, result) {
    handleEvent('success', worker, queue, job, result);
  });
}