exports.Queuemanager = {
  createQueue: function(id, type, options) {
    type = type || 'memory';
    option = options || {};
    // @todo - fix
    var Queue = require('./queuemanager/' + type + '.js').Queue;
    return new Queue(id, options);
  }
}
