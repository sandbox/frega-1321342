var dnode = require('dnode');
var fs = require('fs');
var path = require('path');
var _ = require('underscore');

/**
 * Global eventemitter2 instance.
 */
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var emitter = new EventEmitter2({
  wildcard: true // should the event emitter use wildcards.
});

/**
 * Helper function merging objects (deep-merge)
 * @param object a
 * @param object b
 * @return object merged objects
 */
function deepOverride( a, b ) {
  var keys =_.keys( b );
  _.each(keys, function( x ) {
    if ( _.isObject(a[x]) && _.isObject(b[x]) ) {
      deepOverride(a[x], b[x]);
    } else {
      a[x] = b[x];
    }
  });
  return a;
}

exports.DrupalDnode = {
  emitter: emitter,
  modules: {},
  servers: {},
  configuration: {},
  /**
   * Connect by serverId (rather than port or domain socket)
   * @param string    serviceId
   * @param callback
   */
  connectByServerId: function( serverId, cb ) {
    // look in the server "registry" to resolve serverId to port/path.
    var p = this.getDnodePortByServerId(serverId);
    if (p===false) {
      cb(new Error('Invalid serverId given'));
    } else {
      return dnode.connect(p , cb );
    }
  },
  /**
   * Return Dnode port by given serverId
   * @param string serverId
   * @return mixed (but normally port)
   */
  getDnodePortByServerId: function(serverId) {
    if (typeof this.modules[serverId] === 'undefined' && !this.configuration[serverId]) {
      return false;
    }
    var module = this.modules[serverId];
    var port;
    // determine port
    if (module.config && typeof module.config.port !== 'undefined') {
      port = module.config.port;
    } else if (this.configuration[serverId] && this.configuration[serverId].port) {
      port = this.configuration[serverId].port;
    } else {
      port = false;
    }
    return port;
  },
  checkPlain: function (str) {
    var character, regex,
        replace = { '&': '&amp;', '"': '&quot;', '<': '&lt;', '>': '&gt;' };
    str = String(str);
    for (character in replace) {
      if (replace.hasOwnProperty(character)) {
        regex = new RegExp(character, 'g');
        str = str.replace(regex, replace[character]);
      }
    }
    return str;
  },
  t: function(string, args, options) {
    // @todo: fetch the localized version of the string.
    if (args) {
      // Transform arguments before inserting them.
      for (var key in args) {
        switch (key.charAt(0)) {
          // Pass-through.
          case '!':
            break;
          case '%':
            // skipping Drupal.theme('placeholder') ...
          case '@':
          default:
            args[key] = this.checkPlain(args[key]);
          break;
        }
        str = str.replace(key, args[key]);
      }
    }
    return str;
  },
  getModuleConfig: function(moduleName) {
    return this.configuration[moduleName] || {};
  },
  requireModules: function(modules, options) {
    var basePath = fs.realpathSync(options.root);
    var file_list = options.modules;
    // require/load modules
    var moduleName;
    for (moduleName in modules) {
      var fn =  modules[moduleName];
      var moduleDir = path.dirname( basePath + '/' + fn );
      if (path.existsSync(moduleDir + '/config.json')) {
        options.debug && console.log('Loading configuration for module ' + moduleName + ' from ' + moduleDir + '/config.json' );
        this.configuration[moduleName] = require(moduleDir + '/config.json');
        //
        if (options.config && options.config[moduleName]) {
          // merge in
          this.configuration[moduleName] = deepOverride(this.configuration[moduleName], options.config[moduleName]);
        }
      } else if (options.config && options.config[moduleName]) {
        this.configuration[moduleName] = options.config[moduleName];
      }
      // load the module
      this.modules[moduleName] = require( basePath + '/' + fn );
    }
  },
  assembleServers: function(options) {
    // let's disable merging of separate modules into one server
    this.servers = this.modules;
  },
  startDnodeServers: function(options) {
    // let's provide a default server bucket ...
    var dnodeServers = {};
    // fire'em up ...
    var moduleName, port;
    for (moduleName in this.modules) {
      var module = this.modules[moduleName];
      var port = this.getDnodePortByServerId(moduleName);
      // If we have a port, let's start the dnode server.
      if (port) {
        dnodeServers[moduleName] = dnode(module);
        dnodeServers[moduleName].listen(port);
        // @todo: expose this via express/connect to the browser directly where applicable ...
        options.debug && console.log( 'Started dnode module/server ' + moduleName + ' on port ' + port );
      } else {
        console.error( 'No dnode port configured for dnode module/server ' + moduleName + ' - no dnode server started.');
      }
    }
    return this.dnodeServers = dnodeServers;
  }
};
