<?php
/**
 * @file
 * dnode drush command.
 */

/**
 * Implements of hook_drush_command().
 */
function dnode_drush_command() {
  $items['dnode-nodejs-servers'] = array(
    'callback' => 'dnode_nodejs_servers',
    'description' => 'Start dnode servers (server side javascript/nodejs)',
    'options' => array(
      'root' => 'Optional root directory to search for *.dnode.js extensions',
      'port' => 'Default port to listen to (defaults to 5050).',
      'uri' => 'Default uri',
    ),
  );
  // RPC.
  $items['dnode-rpc'] = array(
    'callback' => 'dnode_drush_rpc',
    'arguments' => array(
      'server_id' => dt('Server id'),
      'method' => dt('Method to invoke'),
    ),
  );
  // Message.
  $items['dnode-send-message'] = array(
    'callback' => 'dnode_drush_send_message',
    'arguments' => array(
      'event' => dt('Event to emit'),
      'message' => dt('Message data'),
    ),
    'options' => array(
      'strategy' => dt('Optionally force a strategy (dnode_http/dnode_php)'),
      'json' => dt('Message is '),
    ),
  );

  // Test strategies.
  $items['dnode-strategies-test'] = array(
    'callback' => 'dnode_strategies_test',
    'description' => 'Run strategies (and dnodeq) tests',
  );
  // Benchmark strategies.
  $items['dnode-strategies-bench'] = array(
    'callback' => 'dnode_strategies_benchmark',
    'description' => 'Benchmark strategies (synthetic)',
  );
  return $items;
}

/**
 * Drush callback handling the "firing up" of all dnode / nodejs servers,
 * plugins, components etc.
 * @todo: how to background, monit it in a meaning full way etc.
 */
function dnode_nodejs_servers() {
  // Find out drupal root.
  $dnode_dir = drupal_get_path('module', 'dnode');
  $root = drush_get_option('root', drush_locate_root());
  $port = drush_get_option('port', 5050);
  $uri = drush_get_option('uri', FALSE);
  if (!$uri) {
    global $base_url;
    $uri = $base_url;
    if ($uri === 'http://default') {
      drush_print(dt('WARNING: passing the --uri option is recommended when no base_url parameter is configured (settings.php). Some modules may rely on a proper base_url being set!'));
    }
  }
  // Find all declared and active ssjs.
  $info = dnode_get_dnode_info(array('type' => 'ssjs'));
  $module_paths = array();
  // Build the module paths.
  foreach ($info as $entry) {
    $mp = drupal_get_path('module', $entry['module']);
    if (!isset($entry['file'])) {
      $mp .= '/ssjs/' . $entry['module'] . '.dnode.js';
    }
    else {
      $mp .= '/' . $entry['file'];
    }
    $module_paths[$entry['module']] = $mp;
  }
  $modules = escapeshellarg(json_encode($module_paths));
  module_load_include('inc', 'dnode');
  $config = escapeshellarg(json_encode(dnode_get_server_configuration()));
  $x = 'node ' . drush_locate_root() . '/' . $dnode_dir . '/ssjs/dnode-server.js '
    . ' --uri=' . $uri
    . ' --modules=' . $modules
    . ' --config=' . $config
    . ' --root=' . $root . ' --port=' . $port . ' &' ;
  drush_print($x);
  system($x);
}

/**
 * Drush dnode-RPC.
 */
function dnode_drush_rpc() {
  $args = func_get_args();
  $server_id = array_shift($args);
  $method = array_shift($args);
  drush_print(
    dt(
      'Invoking @server_id::@method with !args',
      array(
        '@server_id' => $server_id,
        '@method' => $method,
        '!args' => print_r($args, TRUE),
      )
    )
  );
  dnode_rpc($server_id, $method, $args, function() use ($server_id, $method, $args) {
    drush_print(
      dt(
        'Completed invoking @server_id::@method with !args',
        array(
          '@server_id' => $server_id,
          '@method' => $method,
          '!args' => print_r($args, TRUE),
        )
      )
    );
  });
}

/**
 * Drush dnode-message passing.
 */
function dnode_drush_send_message($event, $message) {
  $json = drush_get_option('json', FALSE);
  if ($json) {
    $message = drupal_json_decode($message);
  }
  drush_print(
    dt(
      'Sending message event @event !message',
      array(
        '@event' => $event,
        '!message' => print_r($message, TRUE),
      )
    )
  );
  dnode_send_message($event, $message);
}


/**
 * Run dnode_http_exampe tests.
 */
function dnode_strategies_test() {
  if (!module_exists('dnodeq')) {
    drush_print(dt('ENABLE dnodeq.module we are testing parts that require dnodeq'));
    return FALSE;
  }
  if (!module_exists('dnode_http') && !module_exists('dnode_php')) {
    drush_print(dt('ENABLE dnode_http.module or dnode_php.module - we should test at least one strategy'));
    return FALSE;
  }
  if ($_SERVER['HTTP_HOST'] == 'default') {
    drush_print(dt('You must call this drush command with --uri'));
    return FALSE;
  }
  // As long as we can't reasonably have SUT and Tester in one
  // ... we'll do it this way. Double meh!
  $test = new LookingForwardToUpalDnodeStrategiesTest();
  if (module_exists('dnode_http')) {
    $test->testDnodeStrategies('dnode_http');
  }
  if (module_exists('dnode_php')) {
    $test->testDnodeStrategies('dnode_php');
  }
}

/**
 * Run dnode strategies tests.
 */
function dnode_strategies_benchmark($strategy = 'dnode_http', $n = 10) {
  drush_print(
    dt('dnode - strategy (synthetic) benchmark - strategy: @strategy',
      array('@strategy' => $strategy)
    )
  );

  $start = timer_start('dnode_strategies_benchmark_async');
  $dnode = dnode_get_dnode_instance(array('strategy' => $strategy));
  $total = 0;
  for ($i = 1; $i <= $n; $i++) {
    $dnode->rpc('dnodeq', 'echoTimesTen', array($i), function($result) use ($total, $i, $n) {
    });
  }

  $stop = timer_stop('dnode_strategies_benchmark_async');
  $time = $stop['time'];
  drush_print(dt("@strategy\t@n\t@time\t@ms_per_test\t@tests_per_ms",
    array(
      '@strategy' => $strategy,
      '@n' => $n,
      '@time'=>$time,
      '@ms_per_test' => round($time / $n, 2),
      '@tests_per_ms' => round($n / $time, 2),
    )
  ));
}
